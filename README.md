# westwing_wishlist

**This is an automation script for WestWing Add a product to Wishlist feature**
The project is divided into 4 sections: 
- keywords: contains common keywords used for the whole project
- resources: consists of library, page object, test key-values and installed robot libraries
- scripts: has robot script to run the test
- testcases: is where test cases stored

Short desciptions of how to run are below, with two possibitities, via IDE or Terminal, targetting MacOS, but for Windows it should be quite similar.

0. Prerequisite: 
    a. Install Chrome Driver: Get latest stable version of ChromeDriver on https://sites.google.com/a/chromium.org/chromedriver/home
    Save it to a system path, ex. /Library/Frameworks/Python.framework/Versions/3.7/bin This system path is then added in the eclipse Preference >  Robot Framework > Installed Frameworks
    b. Set Path environment for ChromeDriver; check if path exists: echo $PATH; add path by command: sudo nano /etc/paths 
    b. Install Robot Framework to IDEs https://marketplace.eclipse.org/content/red-robot-editor http://myqualityassurance.de/2018/02/03/how-to-install-robot-framework-in-pycharm/ 

1. Via IDE (PyCharm or Eclipse)
If used Eclipse, please open Eclipse by command: /Applications/Eclipse.app/Contents/MacOS/eclipse ; exit
    a. Clone and then import the project / Open from File System
    b. From "testcases/wishlist" folder, right-click to the "add_product_to_wishlist.robot" file > Run as > Robot test

2. Via Command line / Terminal
    a. Stand at "westwing_wishlist" folder, type "bash scripts/regression/run.sh"

Test results will be generated under the "results" folder.

Feel free to contact me at doannhumai@gmail.com for further clarification. Thanks for reading.
Mai Doan
