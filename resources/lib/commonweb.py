from robot.libraries.BuiltIn import BuiltIn
from selenium import webdriver
import os
import signal

context = BuiltIn().get_library_instance('SeleniumLibrary')


def verify_element_color(locator, expected_color):
    element = context.find_element(locator)
    actual_color = element.value_of_css_property("background-color")
    print (actual_color)
    if actual_color != expected_color:
        raise AssertionError("Element '%s' is not having expected color '%s'." % (locator,expected_color))


def start_chrome_browser(url):
    options = webdriver.ChromeOptions()
    options.add_experimental_option('w3c', False)
    options.add_argument("--window-size=1440,900")
    context.create_webdriver('Chrome', options=options)
    context.go_to(url)


def start_chrome_browser_dir(url,dir):
    prefs = {
    "download.default_directory": dir,
    "download.prompt_for_download": False,
    "download.directory_upgrade": True
    }
    options = webdriver.ChromeOptions()
    options.add_experimental_option('prefs', prefs)
    options.add_experimental_option('w3c', False)
    context.create_webdriver('Chrome', options=options)
    context.go_to(url)


def start_chrome_headless_browser(url):
    options = webdriver.ChromeOptions()
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-gpu")
    options.add_argument("--disable-features=VizDisplayCompositor")
    options.add_argument("--disable-browser-side-navigation")
    options.accept_untrusted_certs = True
    options.assume_untrusted_cert_issuer = True
    options.add_experimental_option("useAutomationExtension", False)
    options.add_experimental_option("w3c", False)
    options.add_argument("--disable-extensions")
    options.add_argument("--window-size=1440,900")
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--disable-web-security")
    options.add_argument("--allow-running-insecure-content")
    context.create_webdriver("Chrome", options=options)
    context.go_to(url)


def start_chrome_headless_browser_remote(url):
    options = webdriver.ChromeOptions()
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-gpu")
    options.add_argument("--disable-features=VizDisplayCompositor")
    options.add_argument("--disable-browser-side-navigation")
    options.accept_untrusted_certs = True
    options.assume_untrusted_cert_issuer = True
    options.add_experimental_option("useAutomationExtension", False)
    options.add_experimental_option("w3c", False)
    options.add_argument("--disable-extensions")
    options.add_argument("--window-size=1440,900")
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--disable-web-security")
    options.add_argument("--allow-running-insecure-content")
    context.create_webdriver("Remote", options=options, command_executor="http://192.168.0.51:5566/wd/hub")
    context.go_to(url)


def start_chrome_headless_browser_dir(url,dir):
    prefs = {
    "download.default_directory": dir,
    "download.prompt_for_download": False,
    "download.directory_upgrade": True
    }
    options = webdriver.ChromeOptions()
    options.accept_untrusted_certs = True
    options.assume_untrusted_cert_issuer = True
    options.add_experimental_option('w3c', False)
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-extensions")
    # options.add_argument("window-size=1440,900")
    # options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--disable-web-security")
    options.add_argument("--allow-running-insecure-content")
    options.add_experimental_option('prefs', prefs)
    context.create_webdriver('Chrome', options=options)
    current_driver = context.driver
    enable_download_in_headless_chrome(current_driver,dir)
    context.go_to(url)


def enable_download_in_headless_chrome(driver, download_dir):
   #add missing support for chrome "send_command"  to selenium webdriver
   driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')

   params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
   driver.execute("send_command", params)


def kill_all_browsers():
    current_driver = context.driver
    current_driver.close()
    current_driver.quit()


def terminate_all_browsers():
    current_driver = context.driver
    pid = get_current_process_id(current_driver)
    print(pid)
    current_driver.close()
    current_driver.quit()
    kill_chrome_process(pid)


def get_current_process_id(driver):
    process_id = int(driver.service.process.pid)
    return process_id


def kill_chrome_process(pid):
    try:
        os.kill(pid, signal.SIGTERM)
    except OSError:
        return False
    else:
        return True


def table_columns_should_be_correct(locator,column_list):
    column_names = column_list.split(';')
    column_names = [name.strip() for name in column_names]
    length = len(column_names)
    if not context.find_elements(locator + "//thead//th[1]/div//*[@class='ant-checkbox']"):
        for i in range(1, length + 1):
            context.find_element(locator + "//thead//th[" + str(i) + "]/div[text()='" + column_names[i-1] + "']").is_displayed()
    else:
        for i in range(2, length + 1):
            context.find_element(locator + "//thead//th[" + str(i) + "]/div[text()='" + column_names[i-2] + "']").is_displayed()
