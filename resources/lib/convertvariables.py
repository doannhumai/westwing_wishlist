# import sales_registration_var

class ElementDescription():
    key = ''
    xpath = ''

    def __init__(self, key, xpath):
        self.key = key
        self.xpath = xpath

def convert_variables_to_list(var_file):
    source = __import__(var_file)
    list = []
    for attr, value in vars(source).items():
        if (not attr.startswith('__')):
            list.append(ElementDescription(attr, value))
    return list

def seperated_list_by_word(list, key_word):
    des_list = []
    for item in list:
        if key_word in item.key:
            des_list.append(item)
    return des_list

def group_elelemts_by_suffix_word(list, key_word):
    des_list = []
    for item in list:
        if item.key.endswith(key_word):
            des_list.append(item)
    return des_list

