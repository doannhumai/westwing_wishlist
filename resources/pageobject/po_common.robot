*** Settings ***
Resource        ../imports.robot
Resource        ./po_product.robot

*** Variables ***
${search_input}    //input[@type='search']
${wishlist_counter}  //span[contains(@class, 'qa-header-wishlist-counter')]

 
*** Keywords ***
I search for product
    [Arguments]    ${product}
    [Documentation]    Search for a product on the search bar
    Input text to a field  ${search_input}  ${product}
    Press Keys    ${search_input}    ENTER
    Click outside the first register form

The product should be added to the wishlist
    Wishlist icon on the product is filled in
    Wishlist counter in the website header shows 1

Wishlist counter in the website header shows 1
    Wait for element to appear  ${wishlist_counter}
    ${wishlist_no} =  Get number from a locator  ${wishlist_counter}
    log  ${wishlist_no}  INFO
    Should be equal  1  ${wishlist_no}

I go to the wishlist page
    Safely Click Element  ${wishlist_counter}
