*** Settings ***
Resource        ../imports.robot

*** Variables ***
${grid}    //div[starts-with(@class, 'ProductGrid__StyledGrid')]
${wishlist_icon_of_first_element}    //div[starts-with(@class, 'ProductGrid__StyledGrid')]/*[1]//div[starts-with(@class, 'BadgesGrid__BadgesTopRight')]
${wishlist_filled}    //*[starts-with(@class,'ww-uikit_StyledHeartIcon-sc')][contains(@data-is-selected,'true')]
    

*** Keywords ***
I should see product listing page with a list of products
    Wait for element to appear  ${grid}
    
I click on wishlist icon of the first found product
    Safely Click Element  ${wishlist_icon_of_first_element}
    
Wishlist icon on the product is filled in
    ${heart_filled} =  Catenate  ${wishlist_icon_of_first_element} ${wishlist_filled}
    Page should contain element  ${heart_filled}    