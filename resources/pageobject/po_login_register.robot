*** Settings ***
Resource        ../imports.robot

*** Variables ***
${overlay}                       //div[starts-with(@class,'ScrollableArea__StyledScrollableContainer')]
${email_input}	                 //input[@name="email"]
${password_input}                //input[@name="password"]
${login_register_switch_button}  //button[@data-testid='login_reg_switch_btn']
${frame}                         //*[@data-testid='login_and_register']

${login_button}                  //button[.='Anmelden']
${register_button}               //button[.='Registrieren']                        

*** Keywords ***
I should see the login/registration overlay
    Wait for element to appear  ${overlay}
    
I switch to login form of the overlay
    Safely Click Element  ${login_register_switch_button}
    
I log in with credentials
    [Arguments]    ${email}  ${password}
    Wait for element to appear  ${login_button}
    Input text to a field  ${email_input}  ${email}
    Input text to a field  ${password_input}  ${password}
    Safely Click Element  ${login_button}
    Wait for element not to appear on page  ${login_button}
