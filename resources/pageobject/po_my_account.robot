*** Settings ***
Resource        ../imports.robot

*** Variables ***
${wishlist_product_block}  //li[contains(@class, 'qaBlockListProduct')]
${delete_wishlist}  //button[contains(@class, 'qaBlockListProduct__delete')]

*** Keywords ***
# Wishlist section
I delete the product from my wishlist
    Wait for element to appear  ${wishlist_product_block}
    Safely Click Element  ${delete_wishlist}