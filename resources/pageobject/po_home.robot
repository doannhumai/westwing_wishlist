*** Settings ***
Resource        ../imports.robot

*** Variables ***
${cookies_button}    //button[@id='onetrust-accept-btn-handler']
${first_register_email}  //input[@name='email']

*** Keywords ***
I am on the WestwingNow home page
    Open browser ${home_page} with ${browser} browser
