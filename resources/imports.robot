*** Settings ***
# Library  list
Library           SeleniumLibrary      timeout=10.0     implicit_wait=0.0
Library           String
Library           ./lib/convertvariables.py
Library           ./lib/commonweb.py
Library           DateTime


# Resource list
Resource          ../keywords/common_web_keywords.robot

Variables         ./config.yaml
