*** Settings ***
Resource    ../resources/imports.robot
Resource    ../resources/pageobject/po_home.robot   


*** Keywords ***
Open browser ${url} with ${browser} browser
    [Documentation]    supported browsers: chrome, headlesschrome
    ${browser}    convert to lowercase    ${browser}
    Run Keyword If    '${browser}' == 'chrome'        Start Chrome Browser    ${url}
    ...    ELSE IF    '${browser}' == 'headlesschrome'       Start Chrome Headless Browser    ${url}
    ...    ELSE IF    '${browser}' == 'remote'               Start Chrome Headless Browser Remote    ${url}
    ...    ELSE    fail
#   ...    Open Browser      ${url}    ${browser}    desired_capabilities=${desired_capabilities}
    ${present}=   Run Keyword And Return Status    Element Should Be Visible    //div[@data-testid="one-header-icon-my-account"]
    Run Keyword If    ${present}    Click to accept Cookies
    ${register_present}=   Run Keyword And Return Status    Element Should Be Visible    ${first_register_email}
    Run Keyword If    ${register_present}    Click outside the first register form
    ...    ELSE        Wait for element not to appear on page  ${cookies_button}

Click to accept Cookies
    [Documentation]    Accept Cookies for the first time
    Execute JavaScript Click On Element By locator    ${cookies_button}
    
Click outside the first register form
    [Documentation]  This is to close the register form at first time browsing the website
    Run Keyword And Continue On Failure	 Wait Until Page Contains Element  ${first_register_email}
    Run Keyword And Continue On Failure	 Click Element at Coordinates  ${first_register_email}  -400  -200

Execute JavaScript Click On Element By locator
    [Arguments]    ${locator}
    [Documentation]    Execute JavaScript Click On Element By locator
    Wait Until Page Contains Element    ${locator}
    Execute JavaScript    document.evaluate("${locator}", document, null, XPathResult.ANY_TYPE, null).iterateNext().click()

Input text to a field
    [Arguments]     ${element_loc}      ${text}
    [Documentation]    Add a text to a location
    Wait Until Keyword Succeeds     5s    1s    Wait Until Element Is Visible    ${element_loc}    timeout=20s    error=Could not find ${element_loc} element.
    Clear Element Text      ${element_loc}
    Input Text    ${element_loc}    ${text}

Wait for element to appear
    [Arguments]    ${element_loc}
    [Documentation]    Wait for an element to appear
    Wait Until Keyword Succeeds     5s    1s    Wait Until Element Is Visible    ${element_loc}    timeout=20s    error=Could not find ${element_loc} element.

Wait for element not to appear on page
    [Arguments]    ${locator}    ${timeout}=20
    [Documentation]    Try to wait for element at ${locator} with the total timeout of ${timeout}
    Wait Until Keyword Succeeds    ${timeout}s    1s    Element should not be visible    ${locator}
    
Safely Click Element
    [Arguments]    ${locator}
    [Documentation]    Try to click element at ${locator}
    Wait Until Element Is Visible    ${locator}
    ${passed}=    Run Keyword And Return Status    Click Element    ${locator}
    Run Keyword Unless    ${passed}    Execute JavaScript Click On Element By locator    ${locator}

Get number from a locator
    [Arguments]    ${locator}
    [Documentation]    Get actual text number from any types of item
    ${item_value}    Get Element Attribute    ${locator}    value
    ${item_text}    Get text    ${locator}
    ${actual_text}    run keyword if    '${item_value}'=='${NONE}'    set variable    ${item_text}
    ...    ELSE    set variable    ${item_value}
    ${actual_text}       remove string       ${actual_text}       ,
    [Return]    ${actual_text}
