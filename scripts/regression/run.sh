#!/usr/bin/env bash

# ENV here can be divided as dev, qa, staging, production; or by testing purpose such as regression, smoke...
ENV="regression"
RESULT_FOLDER="../../results/$ENV"
TESTCASE_FOLDER="../../testcases/"


## delete screenshot.png before running robot
rm -rf ../../selenium-screenshot*.png
rm -rf $RESULT_FOLDER/selenium-screenshot*.png

## Install Robot libraries
echo "=== $(date) - Installing robot libraries in file requirement.txt ==="
pip3 install -r resources/requirements.txt
pip3 list
echo "=== $(date) Finished installation !!! Done !!! ==="


# Commands to run robot and rerun the failed test cases
echo "=== $(date) - WestWing - Running main job === "
#python3 -m robot.run -L TRACE -v ENV:$ENV -v browser:chrome -d $RESULT_FOLDER -t $TESTCASE_FOLDER
robot -v browser:chrome -d results testcases/wishlist/add_product_to_wishlist.robot
echo "=== $(date) - WestWing - Finished main job !!! Done !!! === "


#echo "=== $(date) - WestWing - Running rerun job with failed test cases ==="
#python3 -m robot.run -L TRACE -v ENV:$ENV -v browser:chrome --rerunfailed $RESULT_FOLDER/output.xml --output $RESULT_FOLDER/rerun.xml $TESTCASE_FOLDER
#echo "=== $(date) - WestWing - Merging results of main and rerun jobs ==="
#python3 -m robot.rebot --merge --output $RESULT_FOLDER/output.xml $RESULT_FOLDER/output.xml $RESULT_FOLDER/rerun.xml
#echo "=== $(date) - WestWing - Finished rerun job !!! Done !!! === "

exit 0