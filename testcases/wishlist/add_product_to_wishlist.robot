*** Settings ***
Resource        ../../resources/pageobject/po_product.robot
Resource        ../../resources/pageobject/po_home.robot
Resource        ../../resources/pageobject/po_common.robot
Resource        ../../resources/pageobject/po_login_register.robot
Resource        ../../resources/pageobject/po_my_account.robot

#Suite Setup    

Suite Teardown    Close all browsers

*** Variables ***

*** Keywords ***

*** Test Cases ***
Add product to wishlist from product page for new customer
    Given I am on the WestwingNow home page
    When I search for product  ${product_mobel}
    Then I should see product listing page with a list of products
    When I click on wishlist icon of the first found product
    Then I should see the login/registration overlay
    When I switch to login form of the overlay
    And I log in with credentials  ${email}  ${password}
    Then The product should be added to the wishlist 
    # (wishlist icon on the product is filled in and wishlist counter in the website header shows 1)
    And I go to the wishlist page
    And I delete the product from my wishlist
    

